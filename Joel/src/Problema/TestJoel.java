package Problema;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class TestJoel {


    @Test
    public void testAtaqueSinDefensaFuego() {
        double resultado = ProblemaJoel.calcularDany(100, "foc", 50, 0, 0, 0);
        assertEquals(0.0, resultado, 0.01);
    }

    @Test
    public void testAtaqueSinDefensaAgua() {
        double resultado = ProblemaJoel.calcularDany(120.25, "aigua", 0.0, 0.0, 0.0, 0.0);
        assertEquals(120.25, resultado, 0.01);
    }

    @Test
    public void testAtaqueConDefensaFuego() {
        double resultado = ProblemaJoel.calcularDany(150.5, "foc", 50.25, 0.0, 0.0, 0.0);
        assertEquals(25.0, resultado, 0.01);
    }

    @Test
    public void testAtaqueConDefensaAgua() {
        double resultado = ProblemaJoel.calcularDany(120.0, "aigua", 0.0, 30.5, 0.0, 0.0);
        assertEquals(29.5, resultado, 0.01);
    }

    @Test
    public void testAtaqueConDefensaTierra() {
        double resultado = ProblemaJoel.calcularDany(200.75, "terra", 0.0, 0.0, 100.25, 50.0);
        assertEquals(0.125, resultado, 0.01);
    }

    @Test
    public void testAtaqueConDefensaAire() {
        double resultado = ProblemaJoel.calcularDany(100.0, "aire", 0.0, 0.0, 0.0, 75.75);
        assertEquals(-25.75, resultado, 0.01);
    }

    @Test
    public void testAtaqueConDefensaMaxima() {
        double resultado = ProblemaJoel.calcularDany(120.0, "foc", 100.5, 100.5, 100.5, 100.5);
        assertEquals(-40.5, resultado, 0.01);
    }

    @Test
    public void testAtaqueTipoInvalido() {
        double resultado = ProblemaJoel.calcularDany(80.75, "veneno", 0.0, 0.0, 0.0, 0.0);
        assertEquals(0.0, resultado, 0.01);
    }

    @Test
    public void testAtaqueConPotenciaBaseCero() {
        double resultado = ProblemaJoel.calcularDany(0.0, "foc", 20.25, 10.5, 0.0, 0.0);
        assertEquals(-20.25, resultado, 0.01);
    }

    @Test
    public void testAtaqueConPotenciaBaseNegativa() {
        double resultado = ProblemaJoel.calcularDany(-50.0, "foc", 10.0, 20.0, 30.0, 40.0);
        assertEquals(-35.0, resultado, 0.01);
    }
}


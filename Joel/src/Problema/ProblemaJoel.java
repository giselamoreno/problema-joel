package Problema;

import java.util.Scanner;

/**
 * Aquesta classe proporciona un mètode per calcular el dany d'un atac.
 */

public class ProblemaJoel {

    /**
     * El mètode principal que sol·licita informació sobre els atacs i calcula el dany final.
     * @param args Els arguments de la línia de comandaments.
     */
	
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        

            // Obtenir les dades de l'atac
        	
            double potenciaBase = sc.nextDouble();
            String tipusAtac = sc.next();
            
            double defensaFoc = sc.nextDouble();
            double defensaAigua = sc.nextDouble();
            double defensaTerra = sc.nextDouble();
            double defensaAire = sc.nextDouble();
            
            // Calcular el dany final de l'atac
            
            double danyFinal = calcularDany(potenciaBase, tipusAtac, defensaFoc, defensaAigua, defensaTerra, defensaAire);
            
            // Mostrar el resultat
      
            
            if (danyFinal > 0) {
                System.out.println("El dany final de l'atac és: " + danyFinal);
            } else {
                System.out.println("L'atac no ha fet dany");
            }
        }
   
    

    /**
     * Calcula el dany final de l'atac basat en la potència base, el tipus d'atac i les defenses de l'enemic.
     * @param potenciaBase La potència base de l'atac.
     * @param tipusAtac El tipus d'atac (foc, aigua, terra, aire).
     * @param defensaFoc La defensa contra atacs de foc.
     * @param defensaAigua La defensa contra atacs d'aigua.
     * @param defensaTerra La defensa contra atacs de terra.
     * @param defensaAire La defensa contra atacs d'aire.
     * @return El dany final de l'atac.
     */
    
    public static double calcularDany(double potenciaBase, String tipusAtac, double defensaFoc, double defensaAigua, double defensaTerra, double defensaAire) {
      
    	double danyFinal = 0;
        
        
        switch (tipusAtac.toLowerCase()) {
            case "foc":
                if (defensaFoc > 0) {
                    danyFinal = (potenciaBase * 0.5) - defensaFoc;
                } else {
                    danyFinal = potenciaBase;
                }
                break;
            case "aigua":
                if (defensaAigua > 0) {
                    danyFinal = (potenciaBase * 0.5) - defensaAigua;
                } else {
                    danyFinal = potenciaBase;
                }
                break;
            case "terra":
                if (defensaTerra > 0) {
                    danyFinal = (potenciaBase * 0.5) - defensaTerra;
                } else {
                    danyFinal = potenciaBase;
                }
                break;
            case "aire":
                if (defensaAire > 0) {
                    danyFinal = (potenciaBase * 0.5) - defensaAire;
                } else {
                    danyFinal = potenciaBase;
                }
                break;
            default:
                System.out.println("Tipus d'atac invàlid");
        }
        
        return danyFinal;
    }
}
